var Bicicleta = require('../../models/bicicleta');
var request = require('request');

describe('Bicicleta API', ()=>{
     describe('GET BICICLETAS /', ()=>{
        it('Status 200', ()=> {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana', [-23.34234,-34.4354]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                 expect(response.statusCode).toBe(200);
            });
        });
     });
});