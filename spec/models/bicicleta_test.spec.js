var Bicicleta = require('../../models/bicicleta');

beforeEach(() => { Bicicleta.allBicis = []; });
describe('Bicicleta.allBicis', ()  =>{
    it('comienza vacia', () => {
       expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicletas.add', () =>{
   it('agregamos una biciclea', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(1, 'rojo','urbana', [-34.6012424,-58.3861497]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);

   });
});

describe('Bicicletas.findById', () =>{
    it('debe devolver la bici con id 1', ()=>{
     expect(Bicicleta.allBicis.length).toBe(0);
 
     var aBici = new Bicicleta(1, 'verde','urbana',);
     var aBici2 = new Bicicleta(1, 'rojo','montaña',);
     Bicicleta.add(aBici);
     Bicicleta.add(aBici2);
 
     var targertBici = Bicicleta.findById(1);
     expect(targertBici.id).toBe(1);
     expect(targertBici.color).toBe(aBici.color);
     expect(targertBici.modelo).toBe(aBici.modelo);   
 
    });
 });